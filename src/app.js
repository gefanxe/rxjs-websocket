import { Subject, ReplaySubject } from "rxjs";
import { concatMap } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import apiRoot from './api';

const myBsub = new Subject();
const replaySub = new ReplaySubject(10);

// 建立連線方式一
// const ws = webSocket('ws://127.0.0.1:8080', );
// const ws = webSocket('ws://10.10.30.75:60000');

// 建立連線方式二
const ws = new WebSocketSubject({
    url: "ws://127.0.0.1:8080",
    // url: "ws://10.10.30.75:60000",
    protocol: 'iamsuperman',
    binaryType: 'arraybuffer',
    deserializer: function(e) {
        // console.log('do deserializer', e);
        return e.data;
    }
});

//多路復用
const A$ = ws.multiplex(
    () => ({ type: 'subscribe', tag: 'A' }),
    () => ({ type: 'unsubscribe', tag: 'A' }),
    message => message.indexOf('A ') >= 0
);

const B$ = ws.multiplex(
    // () => ({ type: 'subscribe', tag: 'B' }),
    function() {
        console.log('filter B');
        return { type: 'subscribe', tag: 'B' };
    },
    () => ({ type: 'unsubscribe', tag: 'B' }),
    message => message.indexOf('B ') >= 0
);


let main = function () {
    let log = document.getElementById('log');
    let logB = document.getElementById('logB');

    let addToLog = function (elem, txt) {
        let addelem = document.createElement("div");
        addelem.innerHTML = txt;
        elem.insertBefore(addelem, elem.firstChild);
    };


    // listen websocket message
    ws.subscribe(res => {

        addToLog(log, res.toString());

    });

    let loginBtn = document.getElementById('loginBtn');
    loginBtn.onclick = function () {
        // TODO: doLogin
        // let testLoginObj = {
        //     "GameID": 1,
        //     "SessionID": "AWS",
        //     "UserID": "demo0002",
        //     "LobbyId": "{\"lobby_id\" : \"lobby01\"}",
        //     "ViaPlatform": 0
        // };

        // ws.next(testLoginObj);


        B$.subscribe(res => {

            addToLog(logB, res.toString());

        });

    };


};
main();

let test = function () {
    let log = document.getElementById('log');
    let testBtn = document.getElementById('test');
    let counter = 0;
    testBtn.onclick = function () {
        let addelem = document.createElement("div");
        addelem.innerHTML = 'test' + counter;
        // log.insertBefore(document.createElement("p"), container.firstChild);
        counter++;
        log.insertBefore(addelem, log.firstChild);
    };

};
// test();
